import os
import sys
sys.path.append(os.path.join(os.getcwd(), "mbed-os"))

from tools.targets import TARGET_MAP
from tools.toolchains.gcc import GCC_ARM

def get_compile_defs(target):
    gcc_arm = GCC_ARM(TARGET_MAP[target])
    return gcc_arm.get_symbols()