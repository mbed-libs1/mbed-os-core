from conans import ConanFile, CMake, tools

MBED_SOURCES = [
    "mbed-os/drivers/*",
    "mbed-os/targets/*",
    "mbed-os/platform/*",
    "mbed-os/rtos/*",
    "mbed-os/rtos/*",
    "mbed-os/cmsis/*",
    "CMakeLists.txt",
    "mbed-os/tools/*",
    "mbed-os/hal/*",
    "mbed_config.h",
    "none.cpp",
    "cmake_helper.py"
]

"""
@note
Required settings during install
compiler=gcc
compiler.version=...
compiler.libcxx=libstdc++11

@note
Required env during install
CC=arm-none-eabi-gcc
CXX=arm-none-eabi-g++
"""
class MbedoscoreConan(ConanFile):
    name = "mbed-os-core"
    version = "5.14.0"
    # TODO: automatic set compiler using env, (Maybe force this through profiles)
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    # TODO: Get options from mbed_lib.json's
    options = {"target" : ["DISCO_F407VG"]}
    exports_sources = MBED_SOURCES
    
    
    def build(self):
        import cmake_helper
        definitions = {}

        #TODO: Change this to the mbed value
        #TODO: Feature: Get target related sources from mbed
        definitions["MBED_TARGET"] = self.options.target
        definitions["CMAKE_C_COMPILER_WORKS"] = True
        definitions["CMAKE_CXX_COMPILER_WORKS"] = True

        # TODO: Get these flags from mbed
        definitions["LD_SYS_LIBS"] =  "-Wl,--start-group -lstdc++ -lsupc++ -lm -lc -lgcc -lnosys  -Wl,--end-group"
        definitions["CMAKE_C_FLAGS"] =  " ".join(["-D" + symbol for symbol in cmake_helper.get_compile_defs(self.options.target.value)]) + " -include mbed_config.h -g3 -std=gnu11 -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -fmessage-length=0 -fno-exceptions -ffunction-sections -fdata-sections -funsigned-char -MMD -fomit-frame-pointer -Og -DMBED_DEBUG -DMBED_TRAP_ERRORS_ENABLED=1 -DMBED_MINIMAL_PRINTF -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -DMBED_ROM_START=0x8000000 -DMBED_ROM_SIZE=0x100000 -DMBED_RAM1_START=0x10000000 -DMBED_RAM1_SIZE=0x10000 -DMBED_RAM_START=0x20000000 -DMBED_RAM_SIZE=0x20000"
        definitions["CMAKE_CXX_FLAGS"] = " ".join(["-D" + symbol for symbol in cmake_helper.get_compile_defs(self.options.target.value)]) + "-fpermissive -include mbed_config.h -g3 -std=gnu++14 -fno-rtti -Wvla -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -fmessage-length=0 -fno-exceptions -ffunction-sections -fdata-sections -funsigned-char -MMD -fomit-frame-pointer -Og -DMBED_DEBUG -DMBED_TRAP_ERRORS_ENABLED=1 -DMBED_MINIMAL_PRINTF -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -DMBED_ROM_START=0x8000000 -DMBED_ROM_SIZE=0x100000 -DMBED_RAM1_START=0x10000000 -DMBED_RAM1_SIZE=0x10000 -DMBED_RAM_START=0x20000000 -DMBED_RAM_SIZE=0x20000"
        definitions["CMAKE_ASM_FLAGS"] =  "-g3 -x assembler-with-cpp -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -fmessage-length=0 -fno-exceptions -ffunction-sections -fdata-sections -funsigned-char -MMD -fomit-frame-pointer -Og -DMBED_DEBUG -DMBED_TRAP_ERRORS_ENABLED=1 -DMBED_MINIMAL_PRINTF -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp "
        definitions["CMAKE_CXX_LINK_FLAGS"] =  "-Wl,--gc-sections -Wl,--wrap,main -Wl,--wrap,__malloc_r -Wl,--wrap,__free_r -Wl,--wrap,__realloc_r -Wl,--wrap,__memalign_r -Wl,--wrap,__calloc_r -Wl,--wrap,exit -Wl,--wrap,atexit -Wl,-n -Wl,--wrap,printf -Wl,--wrap,sprintf -Wl,--wrap,snprintf -Wl,--wrap,vprintf -Wl,--wrap,vsprintf -Wl,--wrap,vsnprintf -Wl,--wrap,fprintf -Wl,--wrap,vfprintf -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -fmessage-length=0 -fno-exceptions -ffunction-sections -fdata-sections -funsigned-char -MMD -fomit-frame-pointer -Og -DMBED_DEBUG -DMBED_TRAP_ERRORS_ENABLED=1 -DMBED_MINIMAL_PRINTF -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -DMBED_ROM_START=0x8000000 -DMBED_ROM_SIZE=0x100000 -DMBED_RAM1_START=0x10000000 -DMBED_RAM1_SIZE=0x10000 -DMBED_RAM_START=0x20000000 -DMBED_RAM_SIZE=0x20000 -DMBED_BOOT_STACK_SIZE=1024 -DXIP_ENABLE=0 "
        definitions["CMAKE_CXX_LINK_FLAGS"] =  "${CMAKE_CXX_LINK_FLAGS} ${LD_SYS_LIBS} -T ${CMAKE_BINARY_DIR}/DISCO_F407VG/GCC_ARM/.link_script.ld"

        cmake = CMake(self, generator="Ninja", set_cmake_flags=True)
        cmake.configure(source_folder=".", defs = definitions)
        cmake.build()

    def package(self):
        self.copy("mbed-os/cmsis/*.h", dst="include", src=".", keep_path=False)
        self.copy("mbed-os/rtos/*.h", dst="include", src=".", keep_path=False)
        self.copy("mbed-os/targets/*.h", dst="include", src=".", keep_path=False)
        self.copy("mbed-os/hal/*.h", dst="include", src=".", keep_path=False)
        self.copy("mbed-os/drivers/*.h", dst="include", src=".", keep_path=False)
        self.copy("mbed-os/platform/*.h", dst="include", src=".", keep_path=False)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["mbed-os-core"]

